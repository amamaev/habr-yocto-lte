FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://habrppp \
            file://habr-chat-connect \
            file://habr-chat-disconnect \
            file://20-habr-modem.rules \
            "

do_install_append () {
	install -m 0755 ${WORKDIR}/habrppp ${D}${sysconfdir}/ppp/peers/habrppp
	install -m 0755 ${WORKDIR}/habr-chat-connect ${D}${sysconfdir}/ppp/peers/habr-chat-connect
	install -m 0755 ${WORKDIR}/habr-chat-disconnect ${D}${sysconfdir}/ppp/peers/habr-chat-disconnect
	mkdir -p ${D}${sysconfdir}/udev/rules.d
	install -m 0755 ${WORKDIR}/20-habr-modem.rules ${D}${sysconfdir}/udev/rules.d/20-habr-modem.rules
}

